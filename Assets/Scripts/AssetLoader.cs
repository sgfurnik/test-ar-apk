﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class AssetLoader : MonoBehaviour
{
    [SerializeField] private float _orcScale;
    [SerializeField] private Transform _targetGroundPlaneTransform;
    [SerializeField] private Button _loadAssetButton;
    [SerializeField] private Shader _orcShader;
    [SerializeField] private Vector3 _orcRotationVector;
    private const string AssetPath = "https://drive.google.com/uc?export=download&id=1M8u1sVyQdQuRG4EWC9yTuWm87UM3Piit";
    private const string LocalAssetPath = "file://C:/TestARApp/orcbundle";
    private const string SkinnedMeshMaterialName = "Grunt_Red";
    private AssetBundle _loadedAssetBundle;
    private GameObject _orcGameObject;
    

    public void LoadAsset()
    {
        StartCoroutine(LoadAssetSequence());
    }

    private IEnumerator LoadAssetSequence()
    {
        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
        using (WWW www = WWW.LoadFromCacheOrDownload(AssetPath, 0))
        {
            _loadAssetButton.interactable = false;
            yield return www;

            if (!String.IsNullOrEmpty(www.error))
            {
                Debug.LogError(www.error);
                _loadAssetButton.interactable = true;
                yield break;
            }
            Debug.Log("AssetLoader: loading completed.");
            _loadAssetButton.gameObject.SetActive(false);

            _loadedAssetBundle = www.assetBundle;
            AssetBundleRequest request = _loadedAssetBundle.LoadAllAssetsAsync();
            yield return request;
            var orcPrefab = GetFromAssetBundle<GameObject>(request, "Orc");
            _orcGameObject = Instantiate(orcPrefab, _targetGroundPlaneTransform);
            InitOrc(_orcGameObject.transform, request);
            //_loadedAssetBundle.Unload(true);
        }
    }

    private void InitOrc(Transform orcTransform, AssetBundleRequest request)
    {
        orcTransform.localPosition = Vector3.zero;
        orcTransform.localScale = new Vector3(_orcScale, _orcScale, _orcScale);
        orcTransform.localRotation = Quaternion.Euler(_orcRotationVector);
        
        var shaderFixer = _orcGameObject.AddComponent<ShaderFixer>();
        shaderFixer.Init(_orcShader);
        shaderFixer.Fix();
    }


    private List<T> GetFromAssetBundle<T>(AssetBundleRequest request) where T : class
    {
        var assetList = new List<T>();
        foreach (var asset in request.allAssets)
        {
            if (asset is T)
            {
                assetList.Add(asset as T);
            }
        }
        if (assetList.Count > 0)
        {
            return assetList;
        }

        Debug.LogErrorFormat("AssetLoader: asset with type {0} not found!", typeof(T));
        return null;
    }

    private T GetFromAssetBundle<T>(AssetBundleRequest request, string name) where T : class
    {
        foreach (var asset in request.allAssets)
        {
            if (asset.name.Equals(name))
            {
                return asset as T;
            }
        }
        Debug.LogErrorFormat("AssetLoader: asset with name {0} not found!", name);
        return null;
    }
}