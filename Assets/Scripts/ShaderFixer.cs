﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderFixer : MonoBehaviour
{
    private Transform _myTransform;
    private SkinnedMeshRenderer _skinnedMeshRenderer;
    private MeshRenderer _shadowMeshRenderer;
    private Shader _orcShader;

    public void Init(Shader orcShader)
    {
        _orcShader = orcShader;
        _myTransform = transform;
            _skinnedMeshRenderer = _myTransform.GetComponentInChildren<SkinnedMeshRenderer>();
        _shadowMeshRenderer = _myTransform.Find("Shadow").GetComponent<MeshRenderer>();
    }

    public void Fix()
    {
        _skinnedMeshRenderer.sharedMaterials[0].shader = _orcShader;
        Debug.Log(_skinnedMeshRenderer.sharedMaterials[0].shader);
        var shadow = _myTransform.Find("Shadow").GetComponent<MeshRenderer>();
        shadow.material.shader = Shader.Find("Sprites/Default");
    }
}